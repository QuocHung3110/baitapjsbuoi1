// Bài 1: bài tập tính số ngày lương
/* Đầu vào:
- lương 1 ngày: 100.000
- số ngày làm: 30 ngày
   Các bước tính toán
- B1: Tạo 2 biến luong1ngay va songaylam cho số tiền lương 1 ngày và ngày làm.
- B2: Tạo biến tongluong cho tổng số lương của nhân viên
- B3: Gán giá trị cho luong1ngay và songaylam
- B4: Sử dụng công thức: tongluong = luong1ngay * songaylam
- B5: in kết quả ra console.
   Đầu ra:
- Tổng lương của nhân viên
 */
var luong1ngay = 100000;
var songaylam = 29;
tongluong = 0;
 
tongluong = luong1ngay * songaylam;
console.log("Lương nhân viên",tongluong);
//  Bài 2: Tính giá trị trung bình
/* Đầu vào:
- 5 số thực gồm : 5, 10, 15, 20, 25.
   Các bước tính toán
- B1: Tạo 5 biến cho 5 số thực
- B2: Tạo biến giatritrungbinh cho tổng 5 số thực trên
- B3: Gán giá trị cho 5 số thực trên
- B4: Sử dụng công thức: giatritrungbinh= (tổng 5 số thực)/5 
- B5: In kết quả ra console.
   Đầu ra:
- Giá trị trung bình của 5 số thực
 */
var number1 = 5;
var number2 = 10;
var number3 = 15;
var number4 = 20;
var number5 = 25;
giatritrungbinh = 0;

giatritrungbinh = (number1 + number2 + number3 + number4 + number5) / 5;
console.log("Giá trị trung bình", giatritrungbinh);
// Bài 3: Quy Đổi Tiền
/* Đầu vào:
- Giá trị của VNĐ
- Số USD muốn quy đổi
   Các bước tính toán:
- B1: Tạo 2 biến giaVND và soluongUSD cho VNĐ và USD
- B2: Tạo biến giaquydoi cho 'Giá trị quy đổi'
- B3: Gán giá trị cho giaVND và soluongUSD
- B4: Sử dụng công thức: giaquydoi = giaVND * soluongUSD
- B5: in kết quả ra console
   Đầu ra:
- Giá trị quy đổi.
 */
var giaVND = 23500;
var soluongUSD = 3;
giaquydoi = 0;

giaquydoi = giaVND * soluongUSD;
console.log("Giá quy đổi",giaquydoi);
// Bài 4: Tính diệ tích, chu vi hình chữ nhật
/* Đầu vào:
- Chiều Dài: 20cm
- chiều rộng: 5cm
   Các bước tính toán:
- B1: Tạo biến chieudai và chieurong cho chiều dài, chiều rộng của hình chữ nhật
- B2: Tạo biến chuvi và dientich cho chu vi, diện tích của hình chữ nhật
- B3: Gán giá trị cho 2 biến chieudai và chieurong
- B4: Sử dụng công thức chuvi = (chieudai + chieurong) * 2 và dientich = chieudai * chieurong 
- B5: In kết quả ra Console
   Đầu ra:
- Chu vi và diện tích của hình chữ nhật 
 */
var chieudai = 20;
var chieurong = 5;
chuvi = 0;
dientich=0;

chuvi = (chieudai + chieurong) * 2;
dientich = chieudai * chieurong;
console.log("Chu vi", chuvi);
console.log("Diện tích", dientich);
// Bài 5: Tính Tổng 2 Ký số
/* Đầu vào:
- Số có 2 chữ số: 25
   Các bước tính toán:
- B1: tạo biến n và gán biến cho số
- B2: tạo biến tong2kyso cho tổng 2 ký số
- B3: tách số hàng chục theo công thức hangchuc = Math.floor (n / 10), hangdonvi = Math.floor (n % 10)
- B4: sử dụng công thức tong2kyso = hangchuc + hangdonvi
- B5: in kết quả ra console.
   Đầu ra:
- Tổng 2 ký số
*/
var n = 25;
tong2kyso = 0;

var hangchuc = Math.floor(n / 10);
var hangdonvi = Math.floor(n % 10);

tong2kyso = hangchuc + hangdonvi;
console.log("Tổng 2 ký số", tong2kyso);